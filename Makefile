LDFLAGS = -lSDL -lGL -lGLU -lGLEW -lm -lc -lgcc -lSDL_image

all: linux

linux:
	g++ src/*.cpp src/grid/grid.a $(LDFLAGS) -o bin/grid

lib:
	$(MAKE) -C src/grid