#include "grid.h"
#include <SDL/SDL.h>
#include <GL/glew.h>

static SDL_Event event;

int Grid::initialize() {
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
		return 1;

	if(SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_OPENGL) < 0)
		return 1;

	// black clear color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glClearDepth(1.0f);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glShadeModel(GL_FLAT);
	
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
		return 1;

	glewInit();

	if(!glewIsSupported("GL_VERSION_2_0"))
		return 1;

	return 0;
}

int Grid::update() {
	while(SDL_PollEvent(&event)) {
		if(event.type == SDL_QUIT)
			return GRID_STOP;
	}

	return GRID_RUN;
}