#ifndef GRID_H
#define GRID_H

#include "gl.h"		// core graphics
#include "chunk.h"	// mesh formed of blocks
#include "world.h"	// collection for chunks

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

// status codes
#define GRID_STOP 0
#define GRID_RUN 1

namespace Grid{
	int initialize();

	int update();
}

#endif