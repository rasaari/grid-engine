#include <GL/glew.h>
#include <SDL/SDL.h>

#include "gl.h"
#include "grid.h"

void Grid::renderScene(void (*callback)(void)) {
	callback();
}

void Grid::render() {
	glViewport(
		0, 
		0, 
		SCREEN_WIDTH, 
		SCREEN_HEIGHT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(
		0, 
		SCREEN_WIDTH, 
		SCREEN_HEIGHT, 
		0, 
		GRID_ORTHO_ZNEAR,
		GRID_ORTHO_ZFAR);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// move camera backward
	glTranslatef(0.0f, 0.0f, -8.0f);

	// render one large quad with screenbuffer as it's texture
	glBegin(GL_QUADS);
		glTexCoord2f(0,1); glVertex3f(10, 10, 0.0f);
		glTexCoord2f(0,0); glVertex3f(10, SCREEN_HEIGHT-10, 0.0f);
		glTexCoord2f(1,0); glVertex3f(SCREEN_WIDTH-10, SCREEN_HEIGHT-10, 0.0f);
		glTexCoord2f(1,1); glVertex3f(SCREEN_WIDTH-10, 10, 0.0f);
	glEnd();

	// display everything
	SDL_GL_SwapBuffers();
}