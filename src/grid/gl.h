#ifndef GRID_GL_H
#define GRID_GL_H

#define GRID_ORTHO_ZNEAR 0.1f
#define GRID_ORTHO_ZFAR 10.0f

namespace Grid {
	void renderScene(void (*callback)(void));
	void render();
}

#endif