#include "grid/grid.h"
#include <iostream>

using namespace std;

void renderScene();

int main(int argc, char** argv) {
	Grid::initialize();

	Grid::World world;

	while(Grid::update() != GRID_STOP) {
		Grid::renderScene(&renderScene);
		Grid::render();
	}

    return 0;
}

void renderScene() {
	cout << "foo" << endl;
}